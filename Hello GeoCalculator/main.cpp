//
//  main.cpp
//  Hello GeoCalculator
//
//  Created by Drink on 29/4/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include <iostream>
#include <array>
#include "shape.hpp"

using namespace std;
using namespace hello_gocalculator;

int main(int argc, const char* argv[]) {

  const array<string, 2> shapes = { "rectangle", "triangle" };

  Shape* shape = nullptr;
  int choice = 0;
  float width = 0;
  float height = 0;

  cout << "This is application for calculate area of geometry" << endl;
  cout << "Which one would you like to calculate" << endl << endl;

  while (choice <= 0 || choice > shapes.size()) {
    for (int i = 0; i < shapes.size(); i++) {
      cout << i + 1 << ")" << shapes[i] << endl;
    }
    cout << endl << "ans: ";
    cin >> choice;
    cout << endl;
  }

  while (width <= 0) {
    cout << "width?" << endl << endl;
    cout << "ans: ";
    cin >> width;
    cout << endl;
  }

  while (height <= 0) {
    cout << "height?" << endl << endl;
    cout << "ans: ";
    cin >> height;
    cout << endl;
  }

  switch (choice) {
  case 1:
    shape = new Rectangle(width, height);
    break;
  case 2:
    shape = new Triangle(width, height);
    break;
  default:
    cout << "oops!!! there is something wrong";
    exit(1);
  }

  shape->draw();

  delete shape;
  return 0;
}
