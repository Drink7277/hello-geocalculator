//
//  Shape.cpp
//  Hello GeoCalculator
//
//  Created by Drink on 29/4/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include <iostream>
#include "shape.hpp"

using namespace std;

namespace hello_gocalculator {

  //Shape

  Shape::Shape(float t_width, float t_height) : m_width(t_width), m_height(t_height) {}

  Shape::~Shape() {}


  //Rectangle

  Rectangle::Rectangle(float t_width, float t_height) :Shape(t_width, t_height) {}

  float Rectangle::area() {
    return m_width * m_height;
  }

  void Rectangle::draw() {
    cout << "+----------+" << endl;
    cout << "|          |" << endl;
    cout << "|          |" << endl;
    cout << "|          |" << endl;
    cout << "|          |" << endl;
    cout << "+----------+" << endl;
    cout << "width: " << m_width << endl;
    cout << "height: " << m_height << endl;
    cout << "area: " << area() << endl;
  }



  //Triangle

  Triangle::Triangle(float t_width, float t_height) :Shape(t_width, t_height) {}

  float Triangle::area() {
    return m_width * m_height* 0.5f;
  }

  void Triangle::draw() {
    cout << "     ^     " << endl;
    cout << "    / \\   " << endl;
    cout << "   /   \\  " << endl;
    cout << "  /     \\ " << endl;
    cout << " /       \\" << endl;
    cout << "+---------+" << endl;
    cout << "width: " << m_width << endl;
    cout << "height: " << m_height << endl;
    cout << "area: " << area() << endl;
  }
}
