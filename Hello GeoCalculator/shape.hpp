//
//  Shape.hpp
//  Hello GeoCalculator
//
//  Created by Drink on 29/4/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef hello_gocalculator_shape_hpp
#define hello_gocalculator_shape_hpp

#include <stdio.h>

namespace hello_gocalculator {

  class Shape {
  public:
    Shape(float t_width, float t_height);
    virtual ~Shape();
    virtual float area() = 0;
    virtual void draw() = 0;

  protected:
    float m_width;
    float m_height;
  };

  class Rectangle : public Shape {
  public:
    Rectangle(float t_width, float t_height);
    float area();
    void draw();
  };

  class Triangle : public Shape {
  public:
    Triangle(float t_width, float t_height);
    float area();
    void draw();
  };
}
#endif /* Shape_hpp */
